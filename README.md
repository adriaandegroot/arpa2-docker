# ARPA2-Docker

> Alternative collection of Docker files for ARPA2 purposes

## Purpose

This is a collection of Dockerfiles that provide a full set of
ARPA2 services. There is a single Makefile that does all the
building and will run a suitable swarm as well.

To get started,
 - `make` or `make build`
 - `make swarm`

## Architecture

There are **four** layers here; they do not necessarily build upon each other.

 - *base* is a basic Debian stable system, updated, and then with
   essential build-tools installed.
 - *build/* is a subdirectory with all of the Dockers needed to
   build ARPA2 software. In principle each one installs ARPA2
   software into `/opt/arpa2`; and **copies** files from the
   ARPA2 images that it has as dependencies.
   These dockers use *base*, install some additional build dependencies,
   copy ARPA2 dependencies, and build-and-install.
 - *data/* is a subdirectory with Dockers containing data that lives
   **independently** of the ARPA2 services; for instance, an LDAP
   server, or a proxying nginx. These use the same *base*
   as the *build* Dockers (so that things are up-to-date)
   but just install and run applications (and some example data).
 - *demo/* is a subdirectory with Dockers for specific demonstration
   purposes. They will copy from the relevant **build** to get
   the ARPA2 software, and possibly install additional packages
   for scaffolding. They run ARPA2 services.

## Usage

Once the data docker is built, it can be run as an independent
LDAP server. It contains suitable sample data.

	make swarm-clean  # Maybe, drops all the current stuff
	make swarm-start  # Runs the LDAP server in a container
	# Check the port, below
	ldapsearch -H ldap://localhost:32768/ -x \
		-b "o=arpa2.net,ou=InternetWide" \
		'(objectClass=lifecycleObject)'

See also Makefile documentation.
