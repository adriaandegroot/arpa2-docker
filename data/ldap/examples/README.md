# LDAP Modification Examples

These are LDIF fragments that modify the (example) LDAP DIT in order
to demonstrate that syncrepl / subscriptions work. In the future
they will also illustrate typical IdentityHub workflow changes.

## Running the examples

To execute these changes, you need ldapsearch and ldapmodify.
See the top-level `Makefile` for examples of running ldapsearch.
Based on that, you can use

    ldapmodify -h localhost -p $LDAP_PORT \
        -x -w sekreet -D "o=arpa2.net,ou=InternetWide" \
        -f data/ldap/examples/ldif-update-persons1
