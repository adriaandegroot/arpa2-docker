#! /bin/sh

# Configures and fills an LDAP server with (example) data for ARPA2
# demo purposes. Also can scrub a slapcat dump for re-use.

case x`uname` in
	xFreeBSD)
		LDAP_ETC=/usr/local/etc/openldap
		LDAP_RUN=/var/run/openldap
		LDAP_DB=/var/db/openldap-data
		stop_ldap() { service slapd stop ; }
		start_ldap() { service slapd start ; }
		;;
	xLinux)
		# Assume Debian
		LDAP_ETC=/etc/ldap
		LDAP_RUN=/var/run
		LDAP_DB=/var/lib/ldap
		stop_ldap() { pkill slapd ; sleep 2 ; }
		start_ldap() { /usr/sbin/slapd & sleep 2 ; }
		;;
	*)
		echo "! Unknown OS" `uname`
		exit 1
		;;
esac

case x"$1" in
	xconfig)	
		stop_ldap
		mkdir -p $PREFIX$LDAP_ETC $PREFIX$LDAP_ETC/schema/
		cp ldap.conf $PREFIX$LDAP_ETC/ldap.conf
		cp schema/* $PREFIX$LDAP_ETC/schema/
		sed \
			-e "s+@@LDAP_ETC@@+$LDAP_ETC+g" \
			-e "s+@@LDAP_RUN@@+$LDAP_RUN+g" \
			-e "s+@@LDAP_DB@@+$LDAP_DB+g" \
			slapd.conf.in > $PREFIX$LDAP_ETC/slapd.conf
		start_ldap
		;;
	xinitial)
		stop_ldap
		rm $PREFIX$LDAP_DB/*.mdb
		start_ldap
		for f in initial.d/[0-9]*.ldif
		do
			ldapadd -f "$f" -x -w sekreet -D "o=arpa2.net,ou=InternetWide"
		done
		;;
	xscrub)
		FILE="$2"
		test -f "$FILE" || { echo "! <scrub> needs a file." ; exit 1 ; }
		python -c '
from sys import stdin
skipping=False
skip_fields=("structuralObjectClass", "entryUUID", "creatorsName", "createTimestamp", "entryCSN", "modifiersName", "modifyTimestamp")
for line in stdin.readlines():
    if skipping and line.startswith(" "):
        continue
    skipping=any([line.startswith(f+":") for f in skip_fields])
    if not skipping:
        print(line.rstrip())
' < "$FILE"
		;;
	*)
		echo "! Unknown command" "$1"
		exit 1
		;;
esac
