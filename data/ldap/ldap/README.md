# LDAP data

This directory is copied into /etc/ldap in the target system.
It contains
 - slapd configuration
 - schemas
 - sample data

## Quick Setup (FreeBSD)

 - Create a jail.
 - In the jail, install OpenLDAP. 
 - Enable OpenLDAP.
 - Then run the config script from this repository, twice: 
   once for configuring, once to load data.

```
pkg install openldap-server openldap-client
sh setup.sh config
sh setup.sh initial
```

## Server Setup

The script ldap/setup.sh can be used to configure an LDAP server.
It places a completely unsecure OpenLDAP configuration file
in the correct place for FreeBSD or Debian -- run this in a
Docker or in a jail.

```
   sh setup.sh config
```


## Data Setup

The script can **also** completely replace the data in an LDAP
server with example data for ARPA2. This is destructive!

```
   sh setup.sh initial
```

The existing database is removed. Sample data is loaded.
