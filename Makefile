# ARPA2 LDAP demo tooling
#
# This uses docker to build a sample LDAP server with example data.
# The demo LDAP server has consistent and meaningful demo-data.
#
# The LDAP server is exposed to the host, but the port
# may vary. Run `make swarm-status` to get output showing
# some network information.
#
# To query the demo LDAP server, use:
#
# ldapsearch -h localhost -p $LDAP_PORT -x -b "o=arpa2.net,ou=InternetWide"
#
# Where LDAP_PORT comes from swarm-status.

all: docker-data-ldap

docker-data-ldap:
	docker build -t arpa2-data-ldap data/ldap

start:
	docker container run -P --detach --name arpa2-data-ldap arpa2-data-ldap:latest

LDAP_PORT_CMD=docker container port arpa2-data-ldap | grep ^1388/ | sed 's/.*://'

status:
	@echo LDAP_PORT=`$(LDAP_PORT_CMD)`

search:
	@LDAP_PORT=`$(LDAP_PORT_CMD)` ; \
	ldapsearch -H "ldap://localhost:$$LDAP_PORT/" -x -b "o=arpa2.net,ou=InternetWide"

stop:
	docker container stop arpa2-data-ldap

clean:
	docker container rm arpa2-data-ldap
